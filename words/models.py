import uuid

from django.db import models


class WordType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text='ID', verbose_name="ID")
    word_type = models.CharField(verbose_name="Word Type", max_length=30, null=False, blank=False, default="")
    word_abbr = models.CharField(verbose_name="Word Abbreviation", max_length=10, null=False, blank=False, default="")

    def __str__(self):
        return self.word_type


class Word(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text='ID', verbose_name="ID")
    word = models.CharField(verbose_name="Word", max_length=30, null=False, blank=False, default="")
    word_type = models.ForeignKey(WordType, on_delete=models.CASCADE, null=False, blank=False)
    definition = models.TextField(verbose_name="Description", null=False, blank=False, default="")
    context = models.CharField(verbose_name="Context", max_length=30, null=True, blank=True, default="")

    def __str__(self):
        return self.word
