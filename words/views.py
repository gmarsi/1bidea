from random import randint

from django.http import JsonResponse
from django.views.generic import TemplateView

from words.models import Word


class BaseView(TemplateView):
    """ Vista de Home """
    template_name = 'core/home.html'


class CreateView(TemplateView):
    """Vista para crear ideas"""
    template_name = 'words/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        count = Word.objects.count()
        range_ = self.request.GET.get('range', 6)

        context['words'] = [Word.objects.all()[randint(0, count - 1)] for _ in range(int(range_))]
        context['sections'] = [
            {'section': 'FLIP', 'description': 'Completely flip the normal solution'},
            {'section': 'INTEGRATION', 'description': 'Integrate the idea with other ideas'},
            {'section': 'EXTENSION', 'description': 'Extend the idea'},
            {'section': 'DIFFERENTIATION', 'description': 'Segment the idea'},
            {'section': 'ADDITION', 'description': 'Add some new item'},
            {'section': 'SUBTRACTION', 'description': 'Remove some element'},
            {'section': 'TRANSLATION', 'description': 'Translate an activity with another discipline'},
            {'section': 'INSERTION', 'description': 'Insert an element or practice from another discipline'},
            {'section': 'EXAGGERATION', 'description': 'Make the idea even its most extreme expression'},
        ]

        return context


def update_word(request):
    current = request.POST.get('current', None)
    word = Word.objects.exclude(word=current).order_by('?').first()

    jr = {'word': word.word}
    return JsonResponse(jr)
