import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "idealabs.settings")
django.setup()

# df = pd.read_json('dictionary.json')
# print(type(df))

from words.models import Word, WordType

#
# types = [
#     {
#         'pos': 'noun',
#         'abbr': 'n.'
#     },
#     {
#         'pos': 'preposition',
#         'abbr': 'prep.'
#     },
#     {
#         'pos': 'adjective',
#         'abbr': 'a.'
#     },
#     {
#         'pos': 'verb',
#         'abbr': 'v.'
#     },
#     {
#         'pos': 'adverb',
#         'abbr': 'adv.'
#     },
#     {
#         'pos': '???',
#         'abbr': 'p.'
#     },
#     {
#         'pos': 'interjection',
#         'abbr': 'interj.'
#     },
#     {
#         'pos': 'conjunction',
#         'abbr': 'conj.'
#     },
#     {
#         'pos': 'pronoun',
#         'abbr': 'pron.'
#     }]
#
# for i, line in df.iterrows():
#     word_t = [t['pos'] for t in types if t['abbr'] == line['pos']]
#
#     word_type, created = WordType.objects.get_or_create(word_abbr=line['pos'],
#         word_type=word_t[0])
#
#     word_created, word = Word.objects.get_or_create(word=line['word'], word_type=word_type,
#         definition=line['definitions'][0])

# verb = WordType.objects.get(word_type='verb')
#
# Word.objects.get_or_create(word="to be", word_type=verb, definition="")
# Word.objects.get_or_create(word="to have", word_type=verb, definition="")
# Word.objects.get_or_create(word="to do", word_type=verb, definition="")
# Word.objects.get_or_create(word="to say", word_type=verb, definition="")
# Word.objects.get_or_create(word="to go", word_type=verb, definition="")
# Word.objects.get_or_create(word="to get", word_type=verb, definition="")
# Word.objects.get_or_create(word="to make", word_type=verb, definition="")
# Word.objects.get_or_create(word="to know", word_type=verb, definition="")
# Word.objects.get_or_create(word="to think", word_type=verb, definition="")
# Word.objects.get_or_create(word="to take", word_type=verb, definition="")
# Word.objects.get_or_create(word="to see", word_type=verb, definition="")
# Word.objects.get_or_create(word="to come", word_type=verb, definition="")
# Word.objects.get_or_create(word="to want", word_type=verb, definition="")
# Word.objects.get_or_create(word="to look", word_type=verb, definition="")
# Word.objects.get_or_create(word="to use", word_type=verb, definition="")
# Word.objects.get_or_create(word="to find", word_type=verb, definition="")
# Word.objects.get_or_create(word="to give", word_type=verb, definition="")
# Word.objects.get_or_create(word="to tell", word_type=verb, definition="")
# Word.objects.get_or_create(word="to work", word_type=verb, definition="")
# Word.objects.get_or_create(word="to call", word_type=verb, definition="")
# Word.objects.get_or_create(word="to try", word_type=verb, definition="")
# Word.objects.get_or_create(word="to ask", word_type=verb, definition="")
# Word.objects.get_or_create(word="to need", word_type=verb, definition="")
# Word.objects.get_or_create(word="to feel", word_type=verb, definition="")
# Word.objects.get_or_create(word="to become", word_type=verb, definition="")
# Word.objects.get_or_create(word="to leave", word_type=verb, definition="")
# Word.objects.get_or_create(word="to put", word_type=verb, definition="")
# Word.objects.get_or_create(word="to mean", word_type=verb, definition="")
# Word.objects.get_or_create(word="to keep", word_type=verb, definition="")
# Word.objects.get_or_create(word="to let", word_type=verb, definition="")
# Word.objects.get_or_create(word="to begin", word_type=verb, definition="")
# Word.objects.get_or_create(word="to seem", word_type=verb, definition="")
# Word.objects.get_or_create(word="to help", word_type=verb, definition="")
# Word.objects.get_or_create(word="to talk", word_type=verb, definition="")
# Word.objects.get_or_create(word="to turn", word_type=verb, definition="")
# Word.objects.get_or_create(word="to start", word_type=verb, definition="")
# Word.objects.get_or_create(word="to show", word_type=verb, definition="")
# Word.objects.get_or_create(word="to hear", word_type=verb, definition="")
# Word.objects.get_or_create(word="to play", word_type=verb, definition="")
# Word.objects.get_or_create(word="to run", word_type=verb, definition="")
# Word.objects.get_or_create(word="to move", word_type=verb, definition="")
# Word.objects.get_or_create(word="to like", word_type=verb, definition="")
# Word.objects.get_or_create(word="to live", word_type=verb, definition="")
# Word.objects.get_or_create(word="to believe", word_type=verb, definition="")
# Word.objects.get_or_create(word="to hold", word_type=verb, definition="")
# Word.objects.get_or_create(word="to bring", word_type=verb, definition="")
# Word.objects.get_or_create(word="to happen", word_type=verb, definition="")
# Word.objects.get_or_create(word="to write", word_type=verb, definition="")
# Word.objects.get_or_create(word="to provide", word_type=verb, definition="")
# Word.objects.get_or_create(word="to si", word_type=verb, definition="")
# Word.objects.get_or_create(word="to stand", word_type=verb, definition="")
# Word.objects.get_or_create(word="to lose", word_type=verb, definition="")
# Word.objects.get_or_create(word="to pay", word_type=verb, definition="")
# Word.objects.get_or_create(word="to meet", word_type=verb, definition="")
# Word.objects.get_or_create(word="to include", word_type=verb, definition="")
# Word.objects.get_or_create(word="to continue", word_type=verb, definition="")
# Word.objects.get_or_create(word="to set", word_type=verb, definition="")
# Word.objects.get_or_create(word="to learn", word_type=verb, definition="")
# Word.objects.get_or_create(word="to change", word_type=verb, definition="")
# Word.objects.get_or_create(word="to lead", word_type=verb, definition="")
# Word.objects.get_or_create(word="to understand", word_type=verb, definition="")
# Word.objects.get_or_create(word="to watch", word_type=verb, definition="")
# Word.objects.get_or_create(word="to follow", word_type=verb, definition="")
# Word.objects.get_or_create(word="to stop", word_type=verb, definition="")
# Word.objects.get_or_create(word="to create", word_type=verb, definition="")
# Word.objects.get_or_create(word="to speak", word_type=verb, definition="")
# Word.objects.get_or_create(word="to read", word_type=verb, definition="")
# Word.objects.get_or_create(word="to allow", word_type=verb, definition="")
# Word.objects.get_or_create(word="to add", word_type=verb, definition="")
# Word.objects.get_or_create(word="to spend", word_type=verb, definition="")
# Word.objects.get_or_create(word="to grow", word_type=verb, definition="")
# Word.objects.get_or_create(word="to open", word_type=verb, definition="")
# Word.objects.get_or_create(word="to walk", word_type=verb, definition="")
# Word.objects.get_or_create(word="to win", word_type=verb, definition="")
# Word.objects.get_or_create(word="to offer", word_type=verb, definition="")
# Word.objects.get_or_create(word="to remember", word_type=verb, definition="")
# Word.objects.get_or_create(word="to love", word_type=verb, definition="")
# Word.objects.get_or_create(word="to consider", word_type=verb, definition="")
# Word.objects.get_or_create(word="to appear", word_type=verb, definition="")
# Word.objects.get_or_create(word="to buy", word_type=verb, definition="")
# Word.objects.get_or_create(word="to wait", word_type=verb, definition="")
# Word.objects.get_or_create(word="to serve", word_type=verb, definition="")
# Word.objects.get_or_create(word="to die", word_type=verb, definition="")
# Word.objects.get_or_create(word="to send", word_type=verb, definition="")
# Word.objects.get_or_create(word="to expect", word_type=verb, definition="")
# Word.objects.get_or_create(word="to build", word_type=verb, definition="")
# Word.objects.get_or_create(word="to stay", word_type=verb, definition="")
# Word.objects.get_or_create(word="to fall", word_type=verb, definition="")
# Word.objects.get_or_create(word="to cut", word_type=verb, definition="")
# Word.objects.get_or_create(word="to reach", word_type=verb, definition="")
# Word.objects.get_or_create(word="to kill", word_type=verb, definition="")
# Word.objects.get_or_create(word="to remain", word_type=verb, definition="")
# Word.objects.get_or_create(word="to suggest", word_type=verb, definition="")
# Word.objects.get_or_create(word="to raise", word_type=verb, definition="")
# Word.objects.get_or_create(word="to pass", word_type=verb, definition="")
# Word.objects.get_or_create(word="to sell", word_type=verb, definition="")
# Word.objects.get_or_create(word="to require", word_type=verb, definition="")
# Word.objects.get_or_create(word="to report", word_type=verb, definition="")
# Word.objects.get_or_create(word="to decide", word_type=verb, definition="")
# Word.objects.get_or_create(word="to pul", word_type=verb, definition="")

# import csv
#
# noun = WordType.objects.get(word_type='noun')
#
# with open('nouns.csv') as csv_file:
#     csv_reader = csv.reader(csv_file, delimiter=',')
#     line_count = 0
#     for i, word in enumerate(csv_reader):
#         if i != 0:
#             Word.objects.get_or_create(word=word[0], word_type=noun, definition="")

# Places
noun = WordType.objects.get(word_type='noun')
Word.objects.get_or_create(word="Airport", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Bakery", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Bank", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Bar", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Bookstore", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Bus station", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Butcher's", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Café", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Church", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Court", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Department store", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Cinema", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Service station", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Gym", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Hairdresser's", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Hospital", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Hotel", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Gallery", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Grocery store", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Jail", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Laundromat", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Library", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Mall", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Museum", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Motel", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Parking lot", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Pharmacy", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Police station", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Post office", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Pub", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Park", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Restaurant", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="School", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Train station", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Supermarket", word_type=noun, definition="", context="place")
Word.objects.get_or_create(word="Zoo", word_type=noun, definition="", context="place")

# for word in Word.objects.all():
#     if 'to ' in word.word:
#         print('update', word.word)
#         word.context = 'verb'
#         word.save()
