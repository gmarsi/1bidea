# Generated by Django 3.2.7 on 2021-11-26 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0003_wordtype_word_abbr'),
    ]

    operations = [
        migrations.AddField(
            model_name='word',
            name='context',
            field=models.CharField(blank=True, default='', max_length=30, null=True, verbose_name='Context'),
        ),
    ]
