from django.urls import path
from .views import BaseView, CreateView, update_word

word_patterns = ([
                     path('', BaseView.as_view(), name='base'),
                     path('create', CreateView.as_view(), name='create'),
                     path('update', update_word, name='update'),

                 ], 'core')
